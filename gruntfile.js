// Voor connect task
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

module.exports = function (grunt) {

    grunt.initConfig({

        connect: {
            options: {
                port: 9011,
                // change this to '0.0.0.0' to access the server from outside
                hostname: '0.0.0.0', //'localhost'
                keepalive: true
            },
            dev: {
                options: {
                    middleware: function (connect) {
                        return [
                            mountFolder(connect, 'app')
                            ];
                    }
                }
            },

        },

        open: {
            dev: {
                path: 'http://localhost:<%= connect.options.port %>'
            },
        },



    });


    grunt.registerTask('dev', function (target) {
        grunt.task.run([
        'open:dev',
        'connect:dev',

    ]);
    });



    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-open');

    // Default task(s).
    grunt.registerTask('default', ['dev']);

};