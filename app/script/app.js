/**
 * Util module
 * @return DOM helper
 */
var Util = (function ($) {
	'use strict';

	return {
		byId: function byId(id) {
			return document.getElementById(id);
		},
		show: function show(elm) {
			elm.style.display = 'block';
		},
		hide: function show(elm) {
			elm.style.display = 'none';
		}
	};
}());

/**
 * Distance application as a module
 * @return application
 */
var App = (function (elm) {
	'use strict';

	var map = new google.maps.Map(elm.byId('map'), {
		zoom: 6,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}),
		geocoder = new google.maps.Geocoder(),
		poiSearchInput,
		poiMarker,
		curMarker,
		recentPOIs = ['utrecht'];

	function initApp() {
		initEventHandlers();
		initGeo();
		if (typeof (Storage) !== "undefined" && localStorage.recentPOIs && localStorage.recentPOIs.length) {
			recentPOIs = JSON.parse(localStorage.recentPOIs); //read recentPois from local storage if possible
		}
	}

	/**
	 * Bind event listeners to DOM
	 */
	function initEventHandlers() {
		poiSearchInput = elm.byId('address_poi');
		poiSearchInput.addEventListener('search', calculateDistance);
		poiSearchInput.addEventListener('input', autocomplete);
		//poiSearchInput.addEventListener('focus', autocomplete);
		poiSearchInput.addEventListener("keydown", function (e) {
			if (e.keyCode === 9) { //tab, select first list item if available
				var list = document.getElementsByTagName('li');
				if (list && list.length > 0) poiSearchInput.value = list[0].innerHTML;
				calculateDistance();
			}
			if (e.keyCode === 13) calculateDistance(); //fix for firefox doesnt support 'search' event
		});
		poiSearchInput.addEventListener('blur', function (ev) {
			elm.hide(elm.byId('autocomplete'));
		});
		elm.byId('autocomplete').addEventListener('mousedown', function (ev) {
			if (ev.target.nodeName === 'LI') {
				poiSearchInput.value = ev.target.textContent; //set autocomplete value
				calculateDistance();
			}
		});
	}

	/**
	 * Initialize Google maps
	 */
	function initGeo() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				var location = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				elm.byId('currentLatitude').innerHTML = position.coords.latitude;
				elm.byId('currentLongitude').innerHTML = position.coords.longitude;
				map.setCenter(location);
				curMarker = new google.maps.Marker({
					map: map,
					position: location
				});
				elm.hide(elm.byId('loading'));
			}, function () {
				elm.byId("main").innerHTML = 'Could not retrieve location from your device!, Please try again on a different device.';
				elm.hide(elm.byId('loading'));
			});

		}
	}

	/**
	 * Autocomplete search input based on previous values
	 */
	function autocomplete(ev) {
		var ac = elm.byId('autocomplete'),
			list = "<ul>";
		for (var i = 0; i < recentPOIs.length; i++) {
			if (recentPOIs[i].toLowerCase().indexOf(poiSearchInput.value.toLowerCase()) === 0) {
				list += "<li>" + recentPOIs[i] + "</li>";
			}
		}
		ac.innerHTML = list;
		if (list.length > 4) {
			elm.show(ac);
		} else {
			elm.hide(ac);
		}
	}

	/**
	 * Calculate distance between current location and give POI
	 */
	function calculateDistance() {
		elm.hide(elm.byId('autocomplete'));
		if (poiSearchInput.value.length < 1) return;
		geocoder.geocode({
			'address': poiSearchInput.value
		}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {

				//update or createpoi marker
				if (poiMarker) {
					poiMarker.setPosition(results[0].geometry.location);
				} else {
					poiMarker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location
					});
				}
				var cur = markerToLatLng(curMarker);
				var poi = markerToLatLng(poiMarker);

				//view all markers on map
				var bounds = new google.maps.LatLngBounds();
				bounds.extend(cur);
				bounds.extend(poi);
				map.fitBounds(bounds);

				//show distance
				var distance = google.maps.geometry.spherical.computeDistanceBetween(cur, poi);
				elm.byId('poi_address').innerHTML = poiSearchInput.value;
				elm.byId('poi_distance').innerHTML = Math.round(distance / 1000, 2);

				//add to search history
				if (recentPOIs.indexOf(poiSearchInput.value.toLowerCase()) < 0) {
					recentPOIs.push(poiSearchInput.value.toLowerCase());
					if (typeof (Storage) !== "undefined") {
						localStorage.recentPOIs = JSON.stringify(recentPOIs);
					}
				}

			} else {
				alert("Could not retreive location: " + status);
			}
		});
	}

	/**
	 * Convert marker to LatLong object
	 */
	function markerToLatLng(marker) {
		return new google.maps.LatLng(marker.position.ob, marker.position.pb);
	}

	return {
		init: initApp
	};
}(Util));

document.addEventListener('DOMContentLoaded', App.init);